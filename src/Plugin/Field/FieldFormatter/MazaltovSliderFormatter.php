<?php

namespace Drupal\mazaltov_slick_slider\Plugin\Field\FieldFormatter;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceFormatterBase;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\paragraphs\ParagraphInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'paragraph_summary' formatter.
 *
 * @FieldFormatter(
 *   id = "mazaltov_slick_slider_formatter",
 *   label = @Translation("Mazaltov Slider Formatter"),
 *   field_types = {
 *     "entity_reference_revisions"
 *   }
 * )
 */
class MazaltovSliderFormatter extends EntityReferenceFormatterBase {

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs an MazaltovSliderFormatter instance.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings settings.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, LoggerChannelFactoryInterface $logger_factory, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->loggerFactory = $logger_factory;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('logger.factory'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    // Field formatter extended from EntityReferenceFormatterBase has
    // method which returns all referenced Entities
    // to iterate and customise rendered data.
    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $entity) {
      if ($entity->id()) {
        $slider = [];
        // Iterate on field_images in paragraph Slider OB
        // which allows to host multiply values.
        foreach ($entity->get('field_images')->referencedEntities() as $entity_image) {
          // Implementation of DI for entityTypeManager see __construct method.
          $view_builder = $this->entityTypeManager->getViewBuilder('paragraph');
          // Each referenced item gets array with rendered images.
          $slider['images'][] = $view_builder->view($entity_image, 'default');
        }

        $slider['slack_type'] = $entity->get('field_slider_type')->value;
        // Prepare data for theme('mazaltov_slick_slider') to be rendered.
        $elements[$delta] = [
          '#theme' => 'mazaltov_slick_slider',
          '#slider' => $slider,
        ];
      }
    }
    // Attach library with js files and css to handle sliders frontend behavior.
    $elements['#attached']['library'][] = 'mazaltov_slick_slider/mazaltov_slick_slider';
    return $elements;
  }

  /**
   * Apply field formatter only for fields that implements ParagraphInterface.
   *
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    $target_type = $field_definition->getSetting('target_type');
    $paragraph_type = \Drupal::entityTypeManager()->getDefinition($target_type);
    if ($paragraph_type) {
      return $paragraph_type->entityClassImplements(ParagraphInterface::class);
    }

    return FALSE;
  }
}
