<?php

namespace Drupal\mazaltov_slick_slider\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\Core\Routing\CurrentRouteMatch;

/**
 * Controller for mazaltov_slick_slider routes.
 */
class SlidersJsonController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $currentRouteMatch;

  /**
   * Constructs a new SlidersJsonController object.
   *
   * @param \Drupal\Core\Routing\CurrentRouteMatch $currentRouteMatch
   *   The current route match.
   */
  public function __construct(CurrentRouteMatch $currentRouteMatch) {
    $this->currentRouteMatch = $currentRouteMatch;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_route_match')
    );
  }

  /**
   * Provide a JSON endpoint at path “/node/{node}/slider”.
   *
   * @return mixed
   *   Json response with slick sliders data.
   */
  public function getSlidersJson($arg = NULL) {
    // Get node from currentRouteMatch.
    /** @var \Drupal\node\NodeInterface $node */
    $node = $this->currentRouteMatch->getParameter('node');
    // Check if node is needed bundle, otherwise return empty array.
    if ($node->bundle() != 'slider_node') {
      return new JsonResponse(['data' => [], 'method' => 'GET']);
    }
    // Collect sliders data in referencing field_slider_ob.
    $data = [];
    foreach ($node->get('field_slider_ob')->referencedEntities() as $f_slider) {
      // Collect array of urls for images in referencing field_images.
      $images = [];
      foreach ($f_slider->get('field_images')->referencedEntities() as $p_image) {
        $images[] = Url::fromUri(
          $p_image->get('field_image')->entity->createFileUrl(FALSE)
        )->toString();
      }
      $data[] = [
        'type'=> $f_slider->get('field_slider_type')->value,
        'images' => $images
      ];
    }

    return new JsonResponse([
      'data' => $data,
      'method' => 'GET'
    ]);
  }
}
