# How to use this module
 - Install this module in your drupal site
 - Go to menu Content/Add content/Slider node/
 - Create node of type Slider Node
   - Specify sliders with amount of images more than 3 to be rendered and visible with slider functionality
   - Pay Attention on to field Slider type it could be regular, centered, variable
   - Those 3 type of Slide are builds with javascript different initialisation of slider.
 - After you created Node slider with images you can hit url /node/{node_id}/slider to get Json

