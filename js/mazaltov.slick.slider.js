(function ($, Drupal, window, document) {

  /**
   * Slick slider carousel implementation.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.mazaltovSlickSlider = {
    attach: function(context, settings) {
      if (context !== document) {
        return;
      }
      $(".regular").slick({
        dots: true,
        infinite: true,
        slidesToShow: 2,
        slidesToScroll: 1
      });
      $(".center").slick({
        dots: true,
        infinite: true,
        centerMode: true,
        slidesToShow: 3,
        slidesToScroll: 3
      });
      $(".variable").slick({
        dots: true,
        infinite: true,
        variableWidth: true
      });
    }
  }
})(jQuery, Drupal, this, this.document);
